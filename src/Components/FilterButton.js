import React from "react";

const FilterButton = ({ content, key, clickSelected, selectd, dynamic }) => (
  <button
    key={key}
    name={content}
    className={`ntj__filter-item ${
      content === selectd ? "ntj__filter-item--selected" : ""
    } ${content.toLowerCase()}`}
    onClick={e => clickSelected(e)}
  >
    {!dynamic && content}
  </button>
);

export default FilterButton;
