import React from "react";

const RaceList = ({ data, key }) => {
  const getCountDown = date => {
    return ((new Date().getTime() - new Date(date).getTime()) / 1000 / 86400)
      .toString()
      .slice(1, 10);
  };

  return (
    <div key={key} className="ntj__events-list">
      <div className="ntj__event">
        <div className={`ntj__event-container ntj__event-container-svg }`}>
          <span className={`${data.EventTypeDesc.toLowerCase()}`} />
        </div>
        <div className="ntj__event-container">
          <div className="ntj__event-container-inner">
            <div className="event-data">{data.EventName}</div>
            <div className="event-data">{data.Venue.Venue}</div>
          </div>
          <div className="event-data event-data-time count-down">
            {`${getCountDown(data.AdvertisedStartTime)} days`}
          </div>
        </div>
      </div>
    </div>
  );
};

export default RaceList;
