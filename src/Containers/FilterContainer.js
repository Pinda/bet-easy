import React, { Component } from "react";
import FillterButton from "../Components/FilterButton";
import Loading from "../Components/Loading";

class FilterContainer extends Component {
  buildFilters = () => {
    const { data } = this.props;

    return data.reduce((acc, value) => {
      if (acc.indexOf(value.EventTypeDesc) === -1) {
        acc.push(value.EventTypeDesc);
      }
      return acc;
    }, []);
  };

  handleClick = ({ target: { name } }) => {
    this.props.handleSeleted(name);
  };

  render() {
    const { data } = this.props;

    if (!data) {
      return <Loading />;
    }
    return (
      <div className="fillter-wrapper">
        <FillterButton
          content="all"
          clickSelected={this.handleClick}
          selectd={this.props.selectd}
        />
        {this.buildFilters().map((item, index) => (
          <FillterButton
            key={index}
            content={item}
            clickSelected={this.handleClick}
            selectd={this.props.selectd}
            dynamic
          />
        ))}
      </div>
    );
  }
}

export default FilterContainer;
