import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Check initial state for filter", () => {
  const div = document.createElement("div");
  const component = ReactDOM.render(<App />, div);
  expect(component.state.selected).toEqual("all");
});
