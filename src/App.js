import React, { Component } from "react";
import "./App.css";
import FilterContainer from "./Containers/FilterContainer";
import RaceList from "./Components/RaceList";
import Loading from "./Components/Loading";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      selected: "all",
      dataSelected: [],
    };
  }

  componentDidMount() {
    this.interval = setInterval(this.fetchData, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  fetchData = () => {
    fetch(
      "https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump"
    )
      .then(response => {
        if (response.status !== 200) {
          console.log(
            `Looks like there was a problem. Status Code: ${response.status}`
          );
          return;
        }
        response.json().then(data => {
          this.setState({ data }, () =>
            this.handleSeleted(this.state.selected)
          );
        });
      })
      .catch(err => {
        console.log(`Fetch Error: ${err}`);
      });
  };

  handleSeleted = selected => {
    const { data } = this.state;
    const dataSelected = data.result.filter(item => {
      if (selected !== "all") {
        return item.EventTypeDesc === selected;
      }
      return true;
    });
    this.setState({ dataSelected, selected });
  };

  render() {
    const { data, dataSelected, selected } = this.state;
    if (!data.result) {
      return <Loading />;
    }

    return (
      <div className="app">
        <div className="app-header">
          <div className="app-logo-main" />
          <div className="app-logo-right" />
        </div>
        <div className="content-wrapper">
          <FilterContainer
            data={data.result}
            handleSeleted={this.handleSeleted}
            selectd={selected}
          />
          {dataSelected.map((item, index) => (
            <RaceList key={index} data={item} />
          ))}
        </div>
      </div>
    );
  }
}

export default App;
